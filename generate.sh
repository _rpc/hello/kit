rm -r ../server/lib/src/generated
rm -r ../client/lib/src/generated
mkdir -p ../server/lib/src/generated/hello_service;
protoc --dart_out=grpc:../server/lib/src/generated/hello_service -I ./ hello_service.proto;
mkdir -p ../client/lib/src/generated/hello_service;
protoc --dart_out=grpc:../client/lib/src/generated/hello_service -I ./ hello_service.proto;
mkdir -p ../server/lib/src/generated/chat_service;

protoc --dart_out=grpc:../server/lib/src/generated/chat_service -I ./ chat_service.proto;
mkdir -p ../client/lib/src/generated/chat_service;
protoc --dart_out=grpc:../client/lib/src/generated/chat_service -I ./ chat_service.proto;
mkdir -p ../server/lib/src/generated/auth_service;

protoc --dart_out=grpc:../server/lib/src/generated/auth_service -I ./ auth_service.proto;
mkdir -p ../client/lib/src/generated/auth_service;
protoc --dart_out=grpc:../client/lib/src/generated/auth_service -I ./ auth_service.proto;
