echo "Using: $(which dart)";
dart --version;
pub global activate protoc_plugin;
sudo apt install -y protobuf-compiler; protoc --version;
echo "Make sure to add ~/.pub-cache/bin to the PATH manually";
