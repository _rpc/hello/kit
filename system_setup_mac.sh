echo "Installing on Mac"
echo "Using: $(which dart)";
dart --version;
flutter pub global activate protoc_plugin;
brew install protobuf; protoc --version;
echo "Make sure to add ~/.pub-cache/bin to the PATH manually";
